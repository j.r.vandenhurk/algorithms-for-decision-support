﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace CinemaPlanningLibrary {
    public class Cinema {
        public readonly int NumberOfRows, NumberOfColumns; // Dimensions of the cinema
        public readonly bool[][] Chairs; // Boolean array indicating which positions have chairs
        public readonly List<ChairNode> Nodes; // List of all nodes
        public readonly ChairNode[,] NodeMap; // Two-dimensional array of all nodes by their chair position

        public Cinema(int numberOfRows, int numberOfColumns, bool[][] chairs) {
            NumberOfRows = numberOfRows;
            NumberOfColumns = numberOfColumns;
            Chairs = chairs;

            Nodes = new List<ChairNode>();
            NodeMap = new ChairNode[numberOfRows, numberOfColumns];

            for (int rowNum = 0; rowNum < numberOfRows; rowNum++) {
                for (int colNum = 0; colNum < numberOfColumns; colNum++) {
                    // If there is no chair here, skip this position
                    if (!Chairs[rowNum][colNum]) continue;

                    ChairNode node = new ChairNode(rowNum, colNum);
                    if (rowNum > 0) {
                        // Connect with nodes within range on row above
                        if (colNum > 0) node.ConnectNodesWithinRange(NodeMap[rowNum - 1, colNum - 1]); // Node north west
                        node.ConnectNodesWithinRange(NodeMap[rowNum - 1, colNum]); // Node north
                        if (colNum + 1 < numberOfColumns) node.ConnectNodesWithinRange(NodeMap[rowNum - 1, colNum + 1]); // Node north east
                    }

                    if (colNum > 1) {
                        // Connect with node two positions to the west, which is within range
                        if (colNum > 2) node.ConnectNodesWithinRange(NodeMap[rowNum, colNum - 2]);

                        // Connect with node to the west, which is within range and directly adjacent
                        ChairNode nodeWest = NodeMap[rowNum, colNum - 1];
                        node.ConnectNodesWithinRange(nodeWest);
                        node.ConnectNodesToSide(nodeWest);
                    }

                    Nodes.Add(node);
                    NodeMap[rowNum, colNum] = node;
                }
            }
        }
    }

    public class ChairNode {
        public readonly int RowNum, ColNum;
        public readonly List<ChairNode> NodesToSides, NodesWithinRange;

        public ChairNode(int rowNum, int colNum) {
            RowNum = rowNum;
            ColNum = colNum;
            NodesToSides = new List<ChairNode>();
            NodesWithinRange = new List<ChairNode>();
        }

        /** Connect nodes that are horizontally adjacent */
        public void ConnectNodesToSide(ChairNode node) {
            if (node == null) return;
            NodesToSides.Add(node);
            node.NodesToSides.Add(this);
        }

        /** Connect nodes that are within the 1.5 meter range each other */
        public void ConnectNodesWithinRange(ChairNode node) {
            if (node == null) return;
            NodesWithinRange.Add(node);
            node.NodesWithinRange.Add(this);
        }
    }
}
