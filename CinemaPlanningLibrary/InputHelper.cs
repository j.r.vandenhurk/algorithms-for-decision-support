﻿using System;
namespace CinemaPlanningLibrary {
    public static class InputHelper {
        /** Read the cinema dimensions and chair positions from the input data */
        public static Cinema ReadCinemaData() {
            // Read cinema dimensions
            int numberOfRows = ReadIntLine();
            int numberOfColumns = ReadIntLine();

            // Fill boolean array indicating which positions have chairs
            bool[][] chairs = new bool[numberOfRows][];
            for (int rowNum = 0; rowNum < numberOfRows; rowNum++) {
                chairs[rowNum] = new bool[numberOfColumns];
                for (int colNum = 0; colNum < numberOfColumns; colNum++) {
                    // Read the next character
                    char c = ReadChar();

                    // Store whether we have a chair in this position
                    if (c == '1') chairs[rowNum][colNum] = true;
                    else if (c == '0') chairs[rowNum][colNum] = false;
                    else throw new Exception(string.Format("Invalid input: expected a '1' or a '0' here, but read '{0}' in chair position ({1}, {2})", c, rowNum, colNum));
                }

                // Move to the next line of input
                Console.ReadLine();
            }

            return new Cinema(numberOfRows, numberOfColumns, chairs);
        }

        /** Read a line consisting of an integer */
        public static int ReadIntLine() {
            return int.Parse(Console.ReadLine());
        }

        /** Read a single character from the console and convert to a char type */
        public static char ReadChar() {
            int charCode = Console.Read();
            if (charCode == -1) throw new Exception(string.Format("Unexpected end of input data"));
            return Convert.ToChar(charCode);
        }

        public static int[] ReadGroupSizes()
        {
            // Read the group sizes
            string numberPerGroupSizeLine = Console.ReadLine();
            string[] numberPerGroupSizeStrings = numberPerGroupSizeLine.Split(" ");
            int[] numberPerGroupSize = new int[8]; // Array indicating the number of groups of each possible size (e.g. numberPerGroupSize[2] is the number of the groups of size 3)
            for (int i = 0; i < numberPerGroupSize.Length; i++) numberPerGroupSize[i] = int.Parse(numberPerGroupSizeStrings[i]);
            return numberPerGroupSize;
        }
    }
}
