﻿using System;

namespace CinemaPlanningLibrary {
    public static class OutputHelper {
        /** Temporary method to output the cinema layout that we have read */
        public static void WriteCinema(Cinema cinema, int bestCinemaNumberOfPeoplePlaced, bool[][] bestCinemaPlacement) {
            string personOrPeople = bestCinemaNumberOfPeoplePlaced == 1 ? "person" : "people";
            Console.WriteLine($"\nBest solution, with {bestCinemaNumberOfPeoplePlaced} {personOrPeople} placed:");

            for (int rowNum = 0; rowNum < cinema.NumberOfRows; rowNum++) {
                for (int colNum = 0; colNum < cinema.NumberOfColumns; colNum++) {
                    bool isFilled = bestCinemaPlacement[rowNum][colNum]; ;
                    if (isFilled) {
                        Console.Write("x");
                    } else {
                        bool hasChair = cinema.Chairs[rowNum][colNum];
                        if (hasChair) Console.Write("1");
                        else Console.Write("0");
                    }
                }
                Console.WriteLine();
            }
        }
    }
}
