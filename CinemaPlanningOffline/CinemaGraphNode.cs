﻿using System.Collections.Generic;

namespace CinemaPlanningOffline {
    class CinemaGraphNode {
        public readonly FullRowOption FullRowOption;
        public readonly List<CinemaGraphNode> NextRowNeighbours;
        public int Potential;

        public CinemaGraphNode(FullRowOption fullRowOption) {
            FullRowOption = fullRowOption;
            NextRowNeighbours = new List<CinemaGraphNode>();
            Potential = -1;
        }

        // Add the arrows for neighbours in the next node to the graph
        public void AddNextRowNeighbour(CinemaGraphNode nextRowNeighbour) {
            NextRowNeighbours.Add(nextRowNeighbour);
        }
    }
}
