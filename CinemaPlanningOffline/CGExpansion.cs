﻿namespace CinemaPlanningOffline {
    // Store data for the cinemagraph algorithm expansion
    class CGExpansion {
        public readonly CinemaGraphNode NewNode;
        public readonly int[] NewNumberOfGroupsPerSize;
        public readonly int NewNumberOfPeoplePlaced;
        public readonly CGExpansionPathStep Path;

        public CGExpansion(CinemaGraphNode newNode, int[] newNumberOfGroupsPerSize, int newNumberOfPeoplePlaced, CGExpansionPathStep path) {
            NewNode = newNode;
            NewNumberOfGroupsPerSize = newNumberOfGroupsPerSize;
            NewNumberOfPeoplePlaced = newNumberOfPeoplePlaced;
            Path = path;
        }
    }

    // Store data for the cinemagraph algorithm expansion path
    class CGExpansionPathStep {
        public readonly CinemaGraphNode Node;
        public readonly CGExpansionPathStep Previous;

        public CGExpansionPathStep(CinemaGraphNode node, CGExpansionPathStep previousNode) {
            Node = node;
            Previous = previousNode;
        }
    }
}
