﻿using CinemaPlanningLibrary;
using System;
using System.Diagnostics;

namespace CinemaPlanningOffline {
    class Program {
        static void Main() {
            // Read the cinema layout from input data
            Cinema cinema = InputHelper.ReadCinemaData();

            // Read the group sizes
            int[] maxNumberOfGroupsPerSize = InputHelper.ReadGroupSizes(); // Array indicating the number of groups of each possible size (e.g. maxNumberOfGroupsPerSize[2] is the number of the groups of size 3)

            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();

            // Store the allowed group sizes and maximum row size
            int largestRowSize = 0;
            for (int i = 0; i < cinema.Chairs.Length; i++) {
                if (cinema.Chairs[i].Length > largestRowSize) largestRowSize = cinema.Chairs[i].Length;
            }

            // Steps
            GroupPlacementGenerator step1 = new GroupPlacementGenerator(largestRowSize); // Generate options for where groups can be placed
            RowPlacementGenerator step2 = new RowPlacementGenerator(step1.RowOptionsPerRowSize, maxNumberOfGroupsPerSize);// Generate options for how rows can be filled
            PotentialPrioritySearch step3 = new PotentialPrioritySearch(cinema, step2, maxNumberOfGroupsPerSize); // Run the algorithm

            OutputHelper.WriteCinema(cinema, step3.BestNumberOfPeoplePlaced, step3.BestPlacements);

            stopwatch.Stop();
            Console.WriteLine("\nProgram complete in {0} s", stopwatch.ElapsedMilliseconds / 1000f);
            Console.ReadLine();
        }
    }
}
