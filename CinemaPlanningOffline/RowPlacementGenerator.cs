﻿using System.Collections.Generic;

namespace CinemaPlanningOffline {
    // This class will construct all possible options to fill a row
    class RowPlacementGenerator {
        LinkedItem<RecursiveRowOption>[] rowFillOptions; // Store the first node of the linked list of options for each row size
        readonly int[] maxNumberOfGroupsPerSize;

        public RowPlacementGenerator(List<List<RecursiveRowOption>> rowOptionsPerRowSize, int[] maxNumberOfGroupsPerSize) {
            this.maxNumberOfGroupsPerSize = maxNumberOfGroupsPerSize;

            bool[] areGroupSizesAllowed = new bool[maxNumberOfGroupsPerSize.Length + 1]; // i 1 is group size 1
            areGroupSizesAllowed[0] = true; // The 0 group should always be allowed
            for (int i = 0; i < maxNumberOfGroupsPerSize.Length; i++) {
                if (maxNumberOfGroupsPerSize[i] > 0) areGroupSizesAllowed[i + 1] = true;
            }

            Preprocess(rowOptionsPerRowSize, areGroupSizesAllowed);
        }

        // Convert the row options into linked lists with only the allowed group sizes
        void Preprocess(List<List<RecursiveRowOption>> rowOptionsPerRowSize, bool[] areGroupSizesAllowed) {
            rowFillOptions = new LinkedItem<RecursiveRowOption>[rowOptionsPerRowSize.Count];

            for (int rowNum = 0; rowNum < rowOptionsPerRowSize.Count; rowNum++) {
                // Add the zero option of the row
                List<RecursiveRowOption> rowOptionsThisRowSize = rowOptionsPerRowSize[rowNum]; // Store the row
                LinkedItem<RecursiveRowOption> previousOption = new LinkedItem<RecursiveRowOption>(rowOptionsThisRowSize[0]);
                rowFillOptions[rowNum] = previousOption;

                // Loop through all available options
                for (int optionNum = 1; optionNum < rowOptionsPerRowSize[rowNum].Count; optionNum++) {
                    int groupSize = rowOptionsThisRowSize[optionNum].GroupSize;

                    // Groups larger than the max group size are never allowed
                    if (groupSize > Config.MaxGroupSize) continue;

                    // Check if the group size is allowed
                    if (areGroupSizesAllowed[groupSize]) {
                        // Create new item
                        LinkedItem<RecursiveRowOption> nextOption = new LinkedItem<RecursiveRowOption>(rowOptionsThisRowSize[optionNum]);

                        // Set the link to the previous item
                        previousOption.SetNext(nextOption);
                        previousOption = nextOption;
                    }
                }
            }
        }

        // Create a new object for a row which can loop over all options
        public RowOptionTree CreateNewRowTree(int rowSize, bool[] chairs) {
            return new RowOptionTree(rowSize, rowFillOptions[rowSize], rowFillOptions, chairs, maxNumberOfGroupsPerSize);
        }
    }
}
