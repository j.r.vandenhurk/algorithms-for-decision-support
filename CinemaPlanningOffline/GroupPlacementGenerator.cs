﻿using System;
using System.Collections.Generic;

namespace CinemaPlanningOffline {
    class GroupPlacementGenerator {
        // Store all possible options in a 2d list, size of the row first, second list available options
        public List<List<RecursiveRowOption>> RowOptionsPerRowSize;
        readonly int maximumRowLength;

        public GroupPlacementGenerator(int maximumRowLength) {
            this.maximumRowLength = maximumRowLength;
            Execute();
        }

        // Fill the list of options with available options
        public void Execute() {
            RowOptionsPerRowSize = new List<List<RecursiveRowOption>>();

            for (int i = 0; i <= maximumRowLength; i++) {
                RowOptionsPerRowSize.Add(GenerateRowOptions(i));
            }
        }

        // Generate all options for a single row
        List<RecursiveRowOption> GenerateRowOptions(int rowLength) {
            List<RecursiveRowOption> options = new List<RecursiveRowOption>();

            // Add the no placement option
            options.Add(new RecursiveRowOption(0, 0, null));

            // Generate for each groupsize up to ten all possible options
            int maxGroupSize = Math.Max(rowLength, Config.MaxGroupSize);
            for (int groupSize = 1; groupSize <= maxGroupSize; groupSize++) {
                for (int rowPosition = 0; rowPosition + groupSize <= rowLength; rowPosition++) {
                    // Calculate the free space on both sides of the placed group
                    FreeRow remainingSpace = null;

                    // Add the empty sub row on the right if there should be one
                    int freeSpaceRight = rowLength - rowPosition - groupSize - Config.SeatsBlockedHorizontally; // The free room remaining on the right
                    if (freeSpaceRight > 0) {
                        remainingSpace = new FreeRow(rowPosition + groupSize + Config.SeatsBlockedHorizontally, freeSpaceRight);
                    }

                    // Add the option to the list
                    options.Add(new RecursiveRowOption(groupSize, rowPosition, remainingSpace));
                }
            }

            return options;
        }
    }
}
