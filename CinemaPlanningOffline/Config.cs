﻿namespace CinemaPlanningOffline {
    static class Config {
        public const int SeatsBlockedHorizontally = 2; //The amount of seats blocked horizontally by
        public const int MaxGroupSize = 8; // The highest possible group size
    }
}
