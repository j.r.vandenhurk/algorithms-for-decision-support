﻿
namespace CinemaPlanningOffline {
    // This class will store the maximum number of groups of each size that are allowed, and the current number of groups of each size that are placed
    class UsedGroups {
        readonly int[] maxNumberOfGroupsPerSize;
        readonly int[] usedNumberOfGroupsPerSize;

        public UsedGroups(int[] maxNumberOfGroupsPerSize) {
            this.maxNumberOfGroupsPerSize = maxNumberOfGroupsPerSize;
            usedNumberOfGroupsPerSize = new int[maxNumberOfGroupsPerSize.Length];
        }

        // Check if the number of groups of the given size is currently over its maximum
        public bool DoesCurrentSizeFit(int size) {
            if (size == 0) return true;
            return usedNumberOfGroupsPerSize[size - 1] <= maxNumberOfGroupsPerSize[size - 1];
        }

        public void AddGroup(int size) {
            if (size == 0) return;
            usedNumberOfGroupsPerSize[size - 1]++;
        }

        public void RemoveGroup(int size) {
            if (size == 0) return;
            usedNumberOfGroupsPerSize[size - 1]--;
        }
    }
}
