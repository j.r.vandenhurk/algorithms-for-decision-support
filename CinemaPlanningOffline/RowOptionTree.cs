﻿using System.Collections.Generic;

namespace CinemaPlanningOffline {
    class RowOptionTree {
        public FullRowOption CurrentOption; // The option that we are currently checking
        public FullRowOption FinalOption; // When there are no more option, this will be the last option that we have checked
        public UsedGroups usedGroups; // Store the object that checks if a group can be added
        public bool IsOutOfOptions;
        readonly int rowSize;
        readonly LinkedItem<RecursiveRowOption> firstOption;
        readonly LinkedItem<RecursiveRowOption>[] rowFillOptions;
        readonly bool[] cinemaRow;
        readonly Stack<LinkedItem<RecursiveRowOption>> nodeStack;

        // Enter rowsize, first available option, and a reference to all nodes
        public RowOptionTree(int rowSize, LinkedItem<RecursiveRowOption> firstOption, LinkedItem<RecursiveRowOption>[] rowFillOptions, bool[] cinemaRow, int[] maxNumberOfGroupsPerSize) {
            this.rowSize = rowSize;
            this.firstOption = firstOption;
            this.rowFillOptions = rowFillOptions;
            this.cinemaRow = cinemaRow;

            usedGroups = new UsedGroups(maxNumberOfGroupsPerSize);
            nodeStack = new Stack<LinkedItem<RecursiveRowOption>>();
            ResetOptions();
        }

        /* Getting options */

        public void GoToNextOption(bool wasPreviousValid) {
            LinkedItem<RecursiveRowOption> currentRowOptionNode = nodeStack.Peek();

            if (wasPreviousValid) GoToFittingFirstChild(currentRowOptionNode);
            else GoToFittingNextSibling(currentRowOptionNode);
        }

        public bool IsFullSolution(LinkedItem<RecursiveRowOption> currentRowOptionNode) {
            return !HasCurrentNodeGotChildren(currentRowOptionNode);
        }

        /** Return to the root */
        public void ResetOptions() {
            // Clear the nodestack 
            while (nodeStack.Count > 0) {
                usedGroups.RemoveGroup(nodeStack.Pop().Item.GroupSize);
            }

            CurrentOption = new FullRowOption(rowSize);
            FinalOption = null;
            IsOutOfOptions = false;

            // Add the first option to the tree
            nodeStack.Push(firstOption);
            AddGroupToCurrentOption(firstOption.Item);
        }



        /* Moving between nodes */

        /// <summary>
        /// Generates first child node of the current node,
        /// or tries go to a sibling if the current node is a leaf.
        /// </summary>
        void GoToFittingFirstChild(LinkedItem<RecursiveRowOption> currentRowOptionNode) {
            // Check if the current node has any children
            if (HasCurrentNodeGotChildren(currentRowOptionNode)) {
                // Move to the first child of the current node
                FreeRow freeRow = currentRowOptionNode.Item.FreeColumn;
                LinkedItem<RecursiveRowOption> newRowOptionNode = rowFillOptions[freeRow.FreeColumnSize];
                nodeStack.Push(newRowOptionNode);
                AddGroupToCurrentOption(newRowOptionNode.Item);

                // If the current option doesn't fit, go to the next sibling of the new node
                if (!DoesCurrentNodeFit(newRowOptionNode.Item)) GoToFittingNextSibling(newRowOptionNode);
            } else {
                // Current node has no children, so go to next sibling instead
                GoToFittingNextSibling(currentRowOptionNode);
            }
        }

        /// <summary>
        /// Generates the next sibling of the current node, 
        /// or tries to go to a sibling of the parent of the current node.
        /// </summary>
        LinkedItem<RecursiveRowOption> GoToFittingNextSibling(LinkedItem<RecursiveRowOption> currentRowOptionNode) {
            // Check if the current node has any siblings left
            if (HasCurrentNodeGotSibling(currentRowOptionNode)) {
                // Remove the current option
                nodeStack.Pop();
                RemoveGroupFromCurrentOption(currentRowOptionNode.Item);

                // Add the next option
                LinkedItem<RecursiveRowOption> newRowOptionNode = currentRowOptionNode.Next;
                AddGroupToCurrentOption(newRowOptionNode.Item);
                nodeStack.Push(newRowOptionNode);

                // If the current option doesn't fit, go to the next sibling of the new node
                if (!DoesCurrentNodeFit(newRowOptionNode.Item) && !IsOutOfOptions) return GoToFittingNextSibling(newRowOptionNode);
                return newRowOptionNode;
            } else {
                // Current node has no more sibling, so to go the next sibling of the parent instead
                return GoToFittingParentNextSibling();
            }
        }

        /// <summary>
        /// Generates the next sibling of the parent of the current node,
        /// or tries to go to a sibling of that sibling.
        /// </summary>
        LinkedItem<RecursiveRowOption> GoToFittingParentNextSibling() {
            LinkedItem<RecursiveRowOption> parentOptionNode = GoToParent();
            if (IsOutOfOptions) return null;

            LinkedItem<RecursiveRowOption> parentSiblingOptionNode = GoToFittingNextSibling(parentOptionNode);
            if (IsOutOfOptions) return null;

            if (!DoesCurrentNodeFit(parentSiblingOptionNode.Item)) return GoToFittingNextSibling(parentSiblingOptionNode);

            return parentSiblingOptionNode;
        }

        LinkedItem<RecursiveRowOption> GoToParent() {
            if (!HasCurrentNodeGotParent()) {
                IsOutOfOptions = true;
                FinalOption = CurrentOption;
                CurrentOption = null;
                return null;
            }

            // Go to parent
            LinkedItem<RecursiveRowOption> previousRowOptionNode = nodeStack.Pop();
            RemoveGroupFromCurrentOption(previousRowOptionNode.Item);

            LinkedItem<RecursiveRowOption> newRowOptionNode = nodeStack.Peek();
            return newRowOptionNode;
        }



        /* Editing the current option */

        /// <summary>
        /// Add a node to the current option array
        /// </summary>
        /// <param name="rowOption"></param>
        void AddGroupToCurrentOption(RecursiveRowOption rowOption) {
            CurrentOption.AddGroup(rowOption);
            usedGroups.AddGroup(rowOption.GroupSize);
        }

        /// <summary>
        /// Remove the last node from the current option array
        /// </summary>
        /// <param name="rowOption"></param>
        void RemoveGroupFromCurrentOption(RecursiveRowOption rowOption) {
            CurrentOption.RemoveGroup(rowOption);
            usedGroups.RemoveGroup(rowOption.GroupSize);
        }


        /* Getting special info */

        /// <summary>
        /// Checks if the current node fits in the given cinema row.
        /// </summary>
        /// <returns></returns>
        bool DoesCurrentNodeFit(RecursiveRowOption rowOption) {
            // Retrieve the last node and calculate the old writer position
            int currentOptionOldFillIndex = CurrentOption.FillIndex - rowOption.RowPosition - rowOption.GroupSize - Config.SeatsBlockedHorizontally;

            // Check if the used group does not cause the cinema to place more groups than allowed of the given size
            if (!usedGroups.DoesCurrentSizeFit(rowOption.GroupSize)) return false;

            // Check if the placed group fits in the cinema row
            int colIndexStart = currentOptionOldFillIndex + rowOption.RowPosition;
            for (int i = 0; i < rowOption.GroupSize; i++) {
                // A group can not be placed if any of the required seats don't exists
                if (!cinemaRow[colIndexStart + i]) return false;
            }

            return true;
        }

        /// <summary>
        /// Checks whether the current node has children.
        /// </summary>
        /// <returns></returns>
        bool HasCurrentNodeGotChildren(LinkedItem<RecursiveRowOption> rowOptionNode) {
            if (rowOptionNode.Item.FreeColumn == null) return false; // No free columns, so this node is a leaf
            else return true; // Free columns, so this node is not a leaf
        }

        /// <summary>
        /// Checks whether the current node has a sibling node after it.
        /// </summary>
        /// <returns></returns>
        bool HasCurrentNodeGotSibling(LinkedItem<RecursiveRowOption> rowOptionNode) {
            // Check whether the current node has a next node on the same level in the tree
            return rowOptionNode.Next != null;
        }

        /// <summary>
        /// Checks whether the current node has a parent.
        /// </summary>
        /// <returns></returns>
        bool HasCurrentNodeGotParent() {
            return nodeStack.Count > 1;
        }
    }
}
