﻿using CinemaPlanningLibrary;

namespace CinemaPlanningOffline {
    class PotentialPrioritySearch {
        public int BestNumberOfPeoplePlaced; // Number of people placed in the best solution so far
        public bool[][] BestPlacements; // The placements in the best solution so far
        readonly CGPriorityQueue expansionQueue;
        readonly Cinema cinema;
        readonly int[] maxNumberOfGroupsPerSize;
        readonly CinemaGraph cinemaGraph;

        public PotentialPrioritySearch(Cinema cinema, RowPlacementGenerator step2And3, int[] maxNumberOfGroupsPerSize) {
            this.cinema = cinema;
            this.maxNumberOfGroupsPerSize = maxNumberOfGroupsPerSize;

            // Get the option tree for each row
            RowOptionTree[] rowOptionTrees = new RowOptionTree[cinema.NumberOfRows];
            for (int rowNum = 0; rowNum < cinema.NumberOfRows; rowNum++) {
                bool[] chairs = cinema.Chairs[rowNum];
                rowOptionTrees[rowNum] = step2And3.CreateNewRowTree(cinema.NumberOfColumns, chairs); // TODO: strip empty seats at start and end of row
            }

            // Create the cinema graph
            cinemaGraph = new CinemaGraph(cinema, rowOptionTrees);

            // Create the priority queue
            expansionQueue = new CGPriorityQueue();

            // Initialise best cinema variables
            BestNumberOfPeoplePlaced = -1;
            BestNumberOfPeoplePlaced = -1;
            BestPlacements = new bool[cinema.NumberOfRows][];

            Execute();
        }

        void Execute() {
            // Add first row nodes to the queue
            Expand(cinemaGraph.SourceNode, new int[Config.MaxGroupSize], 0, null);

            // Go though the queue
            while (expansionQueue.Count > 0) {
                // Check if we already have the optimal solution
                if (BestNumberOfPeoplePlaced >= expansionQueue.GetHighestPriority()) return;

                CGExpansion expansion = expansionQueue.Dequeue();
                Expand(expansion.NewNode, expansion.NewNumberOfGroupsPerSize, expansion.NewNumberOfPeoplePlaced, expansion.Path);
            }
        }

        void Expand(CinemaGraphNode node, int[] numberOfGroupsPerSize, int numberOfPeoplePlaced, CGExpansionPathStep path) {
            // Add each neighbour to the queue, if it does not exceed group restrictions
            for (int i = 0; i < node.NextRowNeighbours.Count; i++) {
                CinemaGraphNode newNeighbour = node.NextRowNeighbours[i];

                // Check if the new neighbour stays within group restrictions
                int[] newNumberOfGroupsPerSize = AddNumbersOfGroupPerSize(numberOfGroupsPerSize, newNeighbour.FullRowOption.NumberOfGroupsPerSize);
                if (!IsNumberOfGroupsPerSizeValid(newNumberOfGroupsPerSize)) continue;

                // Calculate the new number of people placed
                int newNumberOfPeoplePlaced = numberOfPeoplePlaced + newNeighbour.FullRowOption.NumberOfPeoplePlaced;

                // Determine the path to this new neighbour
                CGExpansionPathStep newPath = new CGExpansionPathStep(newNeighbour, path);

                // Check if this a full solution
                if (newNeighbour.NextRowNeighbours.Count == 0) {
                    // Save this solution
                    SaveSolutionIfBest(newNumberOfPeoplePlaced, newPath);
                    return;
                }

                // Enqueue the new expansion info
                int priority = numberOfPeoplePlaced + newNeighbour.Potential;
                if (priority > BestNumberOfPeoplePlaced) expansionQueue.Enqueue(new CGExpansion(newNeighbour, newNumberOfGroupsPerSize, newNumberOfPeoplePlaced, newPath), priority);
            }
        }

        void SaveSolutionIfBest(int numberOfPeoplePlaced, CGExpansionPathStep path) {
            // Only save if this solution is better than the currently saved solution
            if (numberOfPeoplePlaced <= BestNumberOfPeoplePlaced) return;

            // Save the number of people placed
            BestNumberOfPeoplePlaced = numberOfPeoplePlaced;

            // Walk back through the path and save the placements for each row
            for (int rowNum = cinema.NumberOfRows - 1; rowNum >= 0; rowNum--) {
                BestPlacements[rowNum] = path.Node.FullRowOption.Placements;
                path = path.Previous;
            }
        }

        int[] AddNumbersOfGroupPerSize(int[] numberOfGroupsPerSize1, int[] numberOfGroupsPerSize2) {
            int[] newNumberOfGroupsPerSize = new int[Config.MaxGroupSize];
            for (int i = 0; i < Config.MaxGroupSize; i++) {
                newNumberOfGroupsPerSize[i] = numberOfGroupsPerSize1[i] + numberOfGroupsPerSize2[i];
            }
            return newNumberOfGroupsPerSize;
        }

        bool IsNumberOfGroupsPerSizeValid(int[] numberOfGroupsPerSize) {
            for (int i = 0; i < Config.MaxGroupSize; i++) {
                if (numberOfGroupsPerSize[i] > maxNumberOfGroupsPerSize[i]) return false;
            }
            return true;
        }
    }
}
