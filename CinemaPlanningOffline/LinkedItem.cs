﻿namespace CinemaPlanningOffline {
    class LinkedItem<T> {
        public readonly T Item;
        public LinkedItem<T> Next;


        public LinkedItem(T item) {
            Item = item;
        }

        public void SetNext(LinkedItem<T> next) {
            Next = next;
        }
    }
}
