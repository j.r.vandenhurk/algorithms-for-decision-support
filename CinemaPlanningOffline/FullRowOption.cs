﻿namespace CinemaPlanningOffline {
    class FullRowOption {
        public readonly bool[] Placements; // Boolean list indicating which seats are filled
        public readonly int[] NumberOfGroupsPerSize; // Integer list storing the number of groups of each size that this option contains
        public int NumberOfPeoplePlaced; // The number of people that are seated in this option
        public int LatestGroupSize; // The group size of the last added group
        public int LatestGroupStartPosition, LatestGroupNextPosition; // The respective first and last index of the range of seats that we last filled
        public int FillIndex; // The column index that we should start at when we add a new group to this option; i.e. the column index of the leftmost non-blocked chair

        public FullRowOption(int rowSize) {
            Placements = new bool[rowSize];
            NumberOfGroupsPerSize = new int[Config.MaxGroupSize];
            NumberOfPeoplePlaced = 0;
            LatestGroupSize = 0;
            LatestGroupStartPosition = 0;
            LatestGroupNextPosition = 0;
            FillIndex = 0;
        }

        public FullRowOption(bool[] placements, int[] numberOfGroupsPerSize, int numberOfPeoplePlaced, int latestGroupSize, int latestGroupStartPosition, int latestGroupNextPosition, int fillIndex) {
            Placements = placements;
            NumberOfGroupsPerSize = numberOfGroupsPerSize;
            NumberOfPeoplePlaced = numberOfPeoplePlaced;
            LatestGroupSize = latestGroupSize;
            LatestGroupStartPosition = latestGroupStartPosition;
            LatestGroupNextPosition = latestGroupNextPosition;
            FillIndex = fillIndex;
        }

        public void AddGroup(RecursiveRowOption rowOption) {
            // Store the latest group size
            LatestGroupSize = rowOption.GroupSize;

            // If the group size is 0, that means the remainder of this row is empty and we don't have to change anything else
            if (rowOption.GroupSize == 0) return;

            // Update the new fills range
            LatestGroupStartPosition = FillIndex + rowOption.RowPosition;
            LatestGroupNextPosition = LatestGroupStartPosition + rowOption.GroupSize;

            // Add the placed group to the current option
            for (int i = 0; i < rowOption.GroupSize; i++) {
                Placements[LatestGroupStartPosition + i] = true;
            }

            // Update the number of people counter
            NumberOfPeoplePlaced += rowOption.GroupSize;

            // Update the group size counter
            NumberOfGroupsPerSize[rowOption.GroupSize - 1]++;

            // Move the start of the writer
            FillIndex = LatestGroupNextPosition + Config.SeatsBlockedHorizontally;
        }

        public void RemoveGroup(RecursiveRowOption rowOption) {
            // If the group size is 0, we don't have to change anything
            if (rowOption.GroupSize == 0) return;

            // Remove the placed group to the current option
            for (int i = 0; i < rowOption.GroupSize; i++) {
                Placements[FillIndex - 1 - i - Config.SeatsBlockedHorizontally] = false;
            }

            // Update the number of people counter
            NumberOfPeoplePlaced -= rowOption.GroupSize;

            // Update the group size counter
            NumberOfGroupsPerSize[rowOption.GroupSize - 1]--;

            // Move the start of the writer
            FillIndex -= rowOption.RowPosition + rowOption.GroupSize + Config.SeatsBlockedHorizontally;
        }

        public FullRowOption DeepCopy() {
            bool[] placementsCopy = new bool[Placements.Length];
            for (int i = 0; i < Placements.Length; i++) placementsCopy[i] = Placements[i];

            int[] numberOfGroupsPerSizeCopy = new int[NumberOfGroupsPerSize.Length];
            for (int i = 0; i < NumberOfGroupsPerSize.Length; i++) numberOfGroupsPerSizeCopy[i] = NumberOfGroupsPerSize[i];

            return new FullRowOption(placementsCopy, numberOfGroupsPerSizeCopy, NumberOfPeoplePlaced, LatestGroupSize, LatestGroupStartPosition, LatestGroupNextPosition, FillIndex);
        }
    }
}
