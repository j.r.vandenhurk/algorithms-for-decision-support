﻿using CinemaPlanningLibrary;
using System.Collections.Generic;

namespace CinemaPlanningOffline {
    class CinemaGraph {
        public readonly CinemaGraphNode SourceNode;
        readonly Cinema cinema;
        readonly List<CinemaGraphNode>[] nodesPerRow;

        public CinemaGraph(Cinema cinema, RowOptionTree[] rowOptionTrees) {
            this.cinema = cinema;

            // Create the source node of the graph
            SourceNode = new CinemaGraphNode(null);

            // Create all nodes for the graph
            nodesPerRow = new List<CinemaGraphNode>[cinema.NumberOfRows];
            for (int rowNum = 0; rowNum < cinema.NumberOfRows; rowNum++) {
                RowOptionTree rowOptionTree = rowOptionTrees[rowNum];

                List<CinemaGraphNode> rowNodes = new List<CinemaGraphNode>();
                nodesPerRow[rowNum] = rowNodes;

                while (!rowOptionTree.IsOutOfOptions) {
                    FullRowOption fullRowOption = rowOptionTree.CurrentOption.DeepCopy();
                    CinemaGraphNode node = new CinemaGraphNode(fullRowOption);

                    rowNodes.Add(node);
                    rowOptionTree.GoToNextOption(true);
                }
            }

            // Sort nodes per row
            for (int rowNum = 0; rowNum < cinema.NumberOfRows; rowNum++) {
                List<CinemaGraphNode> rowNodes = nodesPerRow[rowNum];
                MergeSortNodeArray(rowNodes, 0, rowNodes.Count);
            }

            // Connect source node to all first row nodes
            List<CinemaGraphNode> firstRowNodes = nodesPerRow[0];
            for (int firstRowNodeIndex = 0; firstRowNodeIndex < firstRowNodes.Count; firstRowNodeIndex++) {
                CinemaGraphNode firstRowNode = firstRowNodes[firstRowNodeIndex];
                SourceNode.AddNextRowNeighbour(firstRowNode);
            }

            // Generate edges for valid neighbours
            for (int rowNum = 0; rowNum < cinema.NumberOfRows - 1; rowNum++) {
                List<CinemaGraphNode> currentRowNodes = nodesPerRow[rowNum];
                List<CinemaGraphNode> nextRowNodes = nodesPerRow[rowNum + 1];

                for (int currentRowNodeIndex = 0; currentRowNodeIndex < currentRowNodes.Count; currentRowNodeIndex++) {
                    CinemaGraphNode currentRowNode = currentRowNodes[currentRowNodeIndex];
                    bool[] blockedChairs = GetBlockedChairs(currentRowNode.FullRowOption.Placements);

                    for (int nextRowNodeIndex = 0; nextRowNodeIndex < nextRowNodes.Count; nextRowNodeIndex++) {
                        CinemaGraphNode nextRowNode = nextRowNodes[nextRowNodeIndex];

                        if (IsValidRowOptionCombination(blockedChairs, nextRowNode.FullRowOption.Placements)) {
                            currentRowNode.AddNextRowNeighbour(nextRowNode);
                        }
                    }
                }
            }

            // Calculate potential for each node
            for (int rowNum = cinema.NumberOfRows - 1; rowNum >= 0; rowNum--) {
                List<CinemaGraphNode> rowNodes = nodesPerRow[rowNum];

                for (int rowNodeIndex = 0; rowNodeIndex < rowNodes.Count; rowNodeIndex++) {
                    CinemaGraphNode node = rowNodes[rowNodeIndex];

                    int highestNeighbourPotential = 0;
                    for (int neighbourIndex = 0; neighbourIndex < node.NextRowNeighbours.Count; neighbourIndex++) {
                        CinemaGraphNode neighbour = node.NextRowNeighbours[neighbourIndex];
                        if (neighbour.Potential > highestNeighbourPotential) highestNeighbourPotential = neighbour.Potential;
                    }

                    node.Potential = node.FullRowOption.NumberOfPeoplePlaced + highestNeighbourPotential;
                }
            }
        }

        // Sort nodes based on the amount of people they place
        void MergeSortNodeArray(List<CinemaGraphNode> nodeArray, int firstIndex, int nextIndex) {
            if (firstIndex + 1 == nextIndex) return;

            int splitIndex = (firstIndex + nextIndex + 1) / 2; // First index of the right part

            // Sort the left and right parts
            MergeSortNodeArray(nodeArray, firstIndex, splitIndex);
            MergeSortNodeArray(nodeArray, splitIndex, nextIndex);

            // Merge the left and right parts
            MergeNodeLists(nodeArray, firstIndex, splitIndex, nextIndex);
        }

        void MergeNodeLists(List<CinemaGraphNode> nodeArr, int firstIndex, int splitIndex, int nextIndex) {
            int leftCount = splitIndex - firstIndex;
            int rightCount = nextIndex - splitIndex;

            // Create temporary arrays
            CinemaGraphNode[] left = new CinemaGraphNode[leftCount];
            CinemaGraphNode[] right = new CinemaGraphNode[rightCount];

            for (int l = 0; l < leftCount; l++) left[l] = nodeArr[firstIndex + l];
            for (int l = 0; l < rightCount; l++) right[l] = nodeArr[splitIndex + l];

            int leftIndex = 0;
            int rightIndex = 0;
            int nodeArrIndex = firstIndex;

            // Merge the temporary arrays back into the original array
            while (leftIndex < leftCount && rightIndex < rightCount) {
                if (left[leftIndex].FullRowOption.NumberOfPeoplePlaced >= right[rightIndex].FullRowOption.NumberOfPeoplePlaced) {
                    nodeArr[nodeArrIndex] = left[leftIndex];
                    leftIndex++;
                } else {
                    nodeArr[nodeArrIndex] = right[rightIndex];
                    rightIndex++;
                }
                nodeArrIndex++;
            }

            // Copy the remaining elements from the left array, if necessary
            while (leftIndex < leftCount) {
                nodeArr[nodeArrIndex] = left[leftIndex];
                leftIndex++;
                nodeArrIndex++;
            }

            // Copy the remaining elements from the right array, if necessary
            while (rightIndex < rightCount) {
                nodeArr[nodeArrIndex] = right[rightIndex];
                rightIndex++;
                nodeArrIndex++;
            }
        }

        /** Check whether this combination of row options is valid given their chair placements */
        bool IsValidRowOptionCombination(bool[] currentRowBlockedChairs, bool[] nextRowPlacements) {
            // TODO: only check new positions instead of all positions
            for (int colNum = 0; colNum < cinema.NumberOfColumns; colNum++) {
                if (nextRowPlacements[colNum] && currentRowBlockedChairs[colNum]) return false;
            }
            return true;
        }

        bool[] GetBlockedChairs(bool[] placements) {
            bool[] blockedChairs = new bool[cinema.NumberOfColumns];
            for (int colNum = 0; colNum < cinema.NumberOfColumns; colNum++) {
                if (placements[colNum]) {
                    // The three chairs below this (diagonally left, straight below and diagonally right) are blocked
                    if (colNum > 0) blockedChairs[colNum - 1] = true;
                    blockedChairs[colNum] = true;
                    if (colNum + 1 < cinema.NumberOfColumns) blockedChairs[colNum + 1] = true;
                }
            }
            return blockedChairs;
        }
    }
}
