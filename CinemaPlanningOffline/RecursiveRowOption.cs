﻿namespace CinemaPlanningOffline {
    // Structure to store where groups can be placed in a row
    class RecursiveRowOption {
        public readonly int GroupSize; // Size of the placed group
        public readonly int RowPosition; // First postion of the placed group
        public readonly FreeRow FreeColumn;

        public RecursiveRowOption(int groupSize, int rowPosition, FreeRow freeColumn) {
            GroupSize = groupSize;
            RowPosition = rowPosition;
            FreeColumn = freeColumn;
        }
    }

    // Structure to store whether there is a free sub row and where it is
    public class FreeRow {
        public int FreeColumnLocation;
        public int FreeColumnSize;

        public FreeRow(int freeColumnLocation, int freeColumnSize) {
            FreeColumnLocation = freeColumnLocation;
            FreeColumnSize = freeColumnSize;
        }
    }
}
