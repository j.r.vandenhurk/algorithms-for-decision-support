﻿using System.Collections.Generic;

namespace CinemaPlanningOffline {
    class CGPriorityQueue {
        readonly List<CGPriorityQueueNode> queue;

        public int Count {
            get => queue.Count;
        }

        public CGPriorityQueue() {
            queue = new List<CGPriorityQueueNode>();
        }



        /* Public methods */

        public void Enqueue(CGExpansion expansion, int priority) {
            CGPriorityQueueNode node = new CGPriorityQueueNode(expansion, priority);
            queue.Add(node);
            HeapifyUp(Count - 1);
        }

        public CGExpansion Dequeue() {
            CGExpansion expansion = queue[0].Expansion;

            // Move node with last index to the root, and HeapifyDown
            int lastIndex = Count - 1;
            queue[0] = queue[lastIndex];
            queue.RemoveAt(lastIndex);
            HeapifyDown(0);

            return expansion;
        }

        public int GetHighestPriority() {
            return queue[0].Priority;
        }



        /* Structure methods */

        void HeapifyDown(int nodeIndex) {
            // Get indices of children
            int leftChildIndex = GetLeftChildIndex(nodeIndex);
            int rightChildIndex = GetRightChildIndex(nodeIndex);

            // Determine the highest priority node from this node and its children
            int highestPriorityNodeIndex = nodeIndex;
            if (leftChildIndex < Count && queue[highestPriorityNodeIndex].Priority < queue[leftChildIndex].Priority) highestPriorityNodeIndex = leftChildIndex;
            if (rightChildIndex < Count && queue[highestPriorityNodeIndex].Priority < queue[rightChildIndex].Priority) highestPriorityNodeIndex = rightChildIndex;

            // If the node is smaller than either or both of its children, swap with the largest child
            if (highestPriorityNodeIndex != nodeIndex) {
                SwapNodes(highestPriorityNodeIndex, nodeIndex);
                HeapifyDown(highestPriorityNodeIndex);
            }
        }

        void HeapifyUp(int nodeIndex) {
            while (nodeIndex > 0) {
                // Determine of this node had a higher priority than its parent
                int parentIndex = GetParentIndex(nodeIndex);
                if (queue[parentIndex].Priority > queue[nodeIndex].Priority) return;

                // The node has a higher priority than its parent, so swap them
                SwapNodes(nodeIndex, parentIndex);
                nodeIndex = parentIndex;
            }
        }



        /* Helper methods */
        void SwapNodes(int nodeIndex1, int nodeIndex2) {
            CGPriorityQueueNode node1 = queue[nodeIndex1];
            queue[nodeIndex1] = queue[nodeIndex2];
            queue[nodeIndex2] = node1;
        }

        int GetParentIndex(int nodeIndex) {
            return (nodeIndex - 1) / 2;
        }
        int GetLeftChildIndex(int nodeIndex) {
            return nodeIndex * 2 + 1;
        }
        int GetRightChildIndex(int nodeIndex) {
            return nodeIndex * 2 + 2;
        }
    }

    class CGPriorityQueueNode {
        public CGExpansion Expansion;
        public int Priority;

        public CGPriorityQueueNode(CGExpansion expansion, int priority) {
            Expansion = expansion;
            Priority = priority;
        }
    }
}
