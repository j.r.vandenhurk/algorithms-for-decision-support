# Cinema Planning Algorithm

Utrecht University

Course: Algorithms for Decision Support

## Authors

- Joris Dral - 5686180
- Jason van den Hurk - 6087590
- Jort van Gorkum - 6142834
- Mats Gottenbos - 6215475
- Menno Klunder - 6227589
- Vince van Noort - 6187021
