﻿using CinemaPlanningLibrary;
using System;

namespace CinemaPlanningOnline {
    class Program {
        static void Main() {
            // Read the cinema layout from input data
            Cinema c = InputHelper.ReadCinemaData();
            OnlineCinema cinema = new OnlineCinema(c.NumberOfRows, c.NumberOfColumns, c.Chairs);

            // Keep reading the sizes of arriving groups, placing them
            while (true) {
                int groupSize = InputHelper.ReadIntLine();
                if (groupSize == 0) break; // A group size of 0 means that there are no more groups
                OnlinePlanner.PlaceGroup(cinema, groupSize, true);
            }
            // Count the amount of nodes that are full. In theory we could keep track of this when placing people,
            // but it's a relatively short operation considering all the placing is much more work.
            int count = cinema.CountFullSeats();
            Console.WriteLine($"{count}");
        }
    }
}
