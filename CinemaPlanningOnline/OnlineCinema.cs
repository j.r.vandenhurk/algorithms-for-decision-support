﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CinemaPlanningOnline {
    public enum OfflineCinemaChair {
        Empty = 1,
        Full = 2,
        Blocked = 3,
        NoChair = 4
    }

    public class OnlineCinema {
        public readonly int NumberOfRows, NumberOfColumns;
        public readonly OfflineCinemaChair[,] nodes;

        public OnlineCinema(int numberOfRows, int numberOfColumns, bool[][] chairs) {
            NumberOfRows = numberOfRows;
            NumberOfColumns = numberOfColumns;
            nodes = new OfflineCinemaChair[NumberOfRows, NumberOfColumns];

            for (int rowNum = 0; rowNum < numberOfRows; rowNum++) {
                for (int colNum = 0; colNum < numberOfColumns; colNum++) {
                    if (chairs[rowNum][colNum] == false) {
                        nodes[rowNum, colNum] = OfflineCinemaChair.NoChair;
                    } else {
                        nodes[rowNum, colNum] = OfflineCinemaChair.Empty;
                    }
                }
            }
        }

        public int CountFullSeats() {
            int count = 0;
            foreach (var rows in nodes) {
                if (rows == OfflineCinemaChair.Full) {
                    count++;
                }
            }
            return count;
        }

        /**
         * Gets the amount of free chairs next to each other, with a max of 8. Also returns
         * if it hits the end of the cinema.
         */
        public (int, bool) GetWidthOfFreeChairs(int row, int column) {

            int width = 0;
            bool isTheEnd = column >= NumberOfColumns;
            while (!isTheEnd && width < 8) {
                bool notFree = nodes[row, column] != OfflineCinemaChair.Empty;
                if (notFree) {
                    break;
                }
                width++;
                column++;
                isTheEnd = column >= NumberOfColumns;
            }

            return (width, isTheEnd);
        }

        /**
         * Find the amount of chairs that are being blocked by placing a group.
         */
        public (int, int) GetBlockedChairs(int row, int column, int groupSize) {
            if (groupSize < 1 || groupSize > 8)
                throw new ArgumentException($"The groupSize parameter should be in the range [1, 8]: {groupSize}");

            int horizontalCounter = 0;
            int verticalCounter = 0;

            // Horizontal counters are always calculated the same.
            horizontalCounter += IsChairFree(row, column - 1);
            horizontalCounter += IsChairFree(row, column - 2);
            horizontalCounter += IsChairFree(row, column + groupSize);
            horizontalCounter += IsChairFree(row, column + groupSize + 1);

            if (groupSize == 1) {
                verticalCounter += IsChairFree(row - 1, column);
                verticalCounter += IsChairFree(row + 1, column);
                verticalCounter += IsChairFree(row - 1, column - 1);
                verticalCounter += IsChairFree(row - 1, column + 1);
                verticalCounter += IsChairFree(row + 1, column - 1);
                verticalCounter += IsChairFree(row + 1, column + 1);
            } else if (groupSize >= 2) {
                verticalCounter += IsChairFree(row - 1, column);
                verticalCounter += IsChairFree(row - 1, column + groupSize - 1);
                verticalCounter += IsChairFree(row + 1, column);
                verticalCounter += IsChairFree(row + 1, column + groupSize - 1);
                verticalCounter += IsChairFree(row - 1, column - 1);
                verticalCounter += IsChairFree(row + 1, column - 1);
                verticalCounter += IsChairFree(row - 1, column + groupSize);
                verticalCounter += IsChairFree(row + 1, column + groupSize);
                for (int i = 1; i <= groupSize - 2; i++) {
                    verticalCounter += IsChairFree(row + 1, column + i);
                    verticalCounter += IsChairFree(row - 1, column + i);
                }
            }

            return (horizontalCounter, verticalCounter);
        }

        /**
         * Enumerator to get all possible group combinations in the cinema. For example, if you have the following cinema:
         * 111
         * 111
         * And you pass the minimumWidth 2, you'll get the following pairs:
         * ((0, 0), 2); ((0, 1), 2);
         * ((1, 0), 2); ((1, 1), 2);
         * Representing all placing where 2 people can be placed.
         */
        public IEnumerable<((int, int), int)> GetGroupSizesPerChair(int minimumWidth) {
            for (int i = 0; i < this.NumberOfRows; i++) {
                for (int j = 0; j < this.NumberOfColumns - minimumWidth + 1; j++) {
                    if (IsChairFree(i, j) == 1) {
                        (int widthOfRow, bool end) = this.GetWidthOfFreeChairs(i, j);
                        if (widthOfRow >= minimumWidth) {
                            yield return ((i, j), widthOfRow);
                        }
                    }
                }
            }
        }

        /**
         * Set people in chairs
         */
        public void SetGroupInChairs(int row, int column, int groupSize) {
            for (int i = 0; i < groupSize; i++) {
                SetPersonInChair(row, column + i);
            }
        }

        /**
         * Place a person in a chair. This also sets those chairs the person blocks to diseased
         */
        protected void SetPersonInChair(int row, int column) {
            nodes[row, column] = OfflineCinemaChair.Full;
            // Two to the left
            SetDiseased(row, column - 1);
            SetDiseased(row, column - 2);
            // Two to the right
            SetDiseased(row, column + 1);
            SetDiseased(row, column + 2);
            // Top and bottom
            SetDiseased(row - 1, column);
            SetDiseased(row + 1, column);
            // Top Left
            SetDiseased(row - 1, column - 1);
            // Top right
            SetDiseased(row - 1, column + 1);
            // Bottom left
            SetDiseased(row + 1, column - 1);
            // Bottom right
            SetDiseased(row + 1, column + 1);
        }

        protected void SetDiseased(int row, int column) {
            if (row < 0 || row >= NumberOfRows || column < 0 || column >= NumberOfColumns) {
                return;
            }

            if (nodes[row, column] != OfflineCinemaChair.NoChair && nodes[row, column] != OfflineCinemaChair.Full) {
                nodes[row, column] = OfflineCinemaChair.Blocked;
            }
        }

        protected int IsChairFree(int row, int column) {
            if (row < 0 || row >= NumberOfRows || column < 0 || column >= NumberOfColumns) {
                return 0;
            }
            return nodes[row, column] == OfflineCinemaChair.Empty ? 1 : 0;
        }
    }
}
