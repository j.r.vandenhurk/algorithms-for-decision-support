﻿using CinemaPlanningLibrary;
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text;

namespace CinemaPlanningOnline
{
    static class Experiments
    {
        static string resourcePrefix = "CinemaPlanningOnline.Cinemas.";
        static List<string> testInstances = new List<string>()
            {
                "test-online.txt",
                "test-online-1.txt",
                "test-online-2.txt",
                "test-online-3.txt",
                "test-online-4.txt",
                "test-online-5.txt",
                "test-online-6.txt",
                "test-online-7.txt",
                "test-online-8.txt",
                "test-online-pathe-arena-hall.txt",
                "test-online-pathe-leidsche-rijn-hall-1.txt",
                "test-online-rembrandt.txt",
                "Kinepolis.Jaarbeurs.hall4.txt",
                "Kinepolis.Jaarbeurs.hall5.txt",
                "Kinepolis.Jaarbeurs.hall8.txt",
                "Kinepolis.Jaarbeurs.hall9.txt",
                "Kinepolis.Jaarbeurs.hall13.txt",
                "Kinepolis.Jaarbeurs.hall14.txt",
                "Pathe.Leidsche_Rijn.hall1.txt",
                "Pathe.Leidsche_Rijn.hall3.txt",
                "TestCases.OnlineA1.txt",
                "TestCases.OnlineA2.txt",
                "TestCases.OnlineA3.txt",
                "TestCases.OnlineA4.txt",
                "TestCases.OnlineA5.txt",
                "TestCases.OnlineA6.txt",
                "TestCases.OnlineA7.txt",
                "TestCases.OnlineA8.txt",
                "TestCases.OnlineA9.txt",
                "TestCases.OnlineA10.txt",
                "TestCases.OnlineA11.txt",
                "TestCases.OnlineA12.txt",
                "TestCases.OnlineA13.txt",
                "TestCases.OnlineA14.txt",
                "TestCases.OnlineA15.txt",
                "TestCases.OnlineA16.txt",
                //"TestCases.OnlineA17.txt",
                //"TestCases.OnlineA18.txt",
            };
        static Assembly assembly = Assembly.GetExecutingAssembly();

        /// <summary>
        /// Sets the input stream for a given test instance.
        /// </summary>
        /// <param name="instance"></param>
        static void SetIn(string instance)
        {
            var resourceName = resourcePrefix + instance;
            var stream = assembly.GetManifestResourceStream(resourceName);
            Console.SetIn(new StreamReader(stream));
        }

        /// <summary>
        /// Read the input data for the online cinema.
        /// </summary>
        public static OnlineCinema ReadOnlineCinemaData() {
            Cinema c = InputHelper.ReadCinemaData();
            return new OnlineCinema(c.NumberOfRows, c.NumberOfColumns, c.Chairs);
        }

        /*
         *  Testing Heuristics with deterministic input.
         */

        /// <summary>
        /// Tests any given Heuristic.
        /// </summary>
        /// <param name="PlaceGroup">The method that seats groups using a Heuristic.</param>
        /// <param name="TESTS_PER_INSTANCE">The amount of tests to average over.</param>
        static void TestHeuristic(Action<OnlineCinema, int> PlaceGroup, int TESTS_PER_INSTANCE = 1)
        {
            foreach (var instance in testInstances)
            {
                double averageCount = 0d;

                for (int t = 0; t < TESTS_PER_INSTANCE; t++)
                {
                    SetIn(instance);

                    // Read the cinema layout from input data
                    OnlineCinema cinema = ReadOnlineCinemaData();

                    // Keep reading the sizes of arriving groups
                    while (true)
                    {
                        int groupSize = InputHelper.ReadIntLine();
                        if (groupSize == 0) break; // A group size of 0 means that there are no more groups
                        PlaceGroup(cinema, groupSize);
                    }

                    averageCount += (1d / (double)TESTS_PER_INSTANCE) * (double)cinema.CountFullSeats();
                }

                Console.WriteLine($"{averageCount}");
            }
        }

        /// <summary>
        /// Tests the Blocked-Chair Heuristic (BCH).
        /// </summary>
        public static void Test_BCH()
        {
            Console.WriteLine("> Started test:\t\t BCH");

            Action<OnlineCinema, int> PlaceGroupDelegate = 
                (cinema, groupSize) => OnlinePlanner.PlaceGroup(cinema, groupSize, false);
            TestHeuristic(PlaceGroupDelegate);

            Console.WriteLine("> Finished test:\t BCH\n");
        }

        /// <summary>
        /// Tests the Weighted Blocked-Chair Heuristic (WBCH).
        /// </summary>
        /// <param name="horizontalWeight">The weight for horizontally blocked chairs.</param>
        /// <param name="verticalWeight">The weight for vertically blocked chairs.</param>
        public static void Test_WBCH(int horizontalWeight, int verticalWeight)
        {
            Console.WriteLine("> Started test:\t\t WBCH");

            Action<OnlineCinema, int> PlaceGroupDelegate = 
                (cinema, groupSize) => OnlinePlanner.PlaceGroupWeighted(cinema, groupSize, horizontalWeight, verticalWeight, false);
            TestHeuristic(PlaceGroupDelegate);

            Console.WriteLine("> Finished test:\t WBCH\n");
        }

        /// <summary>
        /// Tests the Random Heuristic (RH).
        /// </summary>
        /// <param name="r">A Random number generator.</param>
        public static void Test_RH(Random r)
        {
            Console.WriteLine("> Started test:\t\t RH");

            Action<OnlineCinema, int> PlaceGroupDelegate = 
                (cinema, groupSize) => OnlinePlanner.PlaceGroupRandomly(cinema, groupSize, r, false);
            TestHeuristic(PlaceGroupDelegate, 100);

            Console.WriteLine("> Finished test:\t RH\n");
        }

        /*
         * Testing Heuristics with input according to probabilities.
         */

        /// <summary>
        /// Generate a groupsize according to a probability distribution.
        /// </summary>
        /// <param name="r">An instance of a random number generator.</param>
        /// <returns>The groupsize.</returns>
        static int GenerateGroup(Random r)
        {
            double[] distribution = { 0.2d, 0.2d, 0.2d, 0.1d, 0.1d, 0.1d, 0.05d, 0.05d };

            double p_ = r.NextDouble();

            double sum = 0d;
            for (int i = 0; i < distribution.Length; i++)
            {
                sum += distribution[i];

                if (p_ < sum)
                    return i + 1;
            }

            throw new Exception($"Either the distribution or generated probability is wrong.");
        }

        /// <summary>
        /// Tests any given Heuristic, but the group input is decided according to probabilities.
        /// </summary>
        /// <param name="r">A Random number generator</param>
        /// <param name="PlaceGroup">The method that seats groups using a Heuristic.</param>
        /// <param name="TESTS_PER_INSTANCE">The amount of tests to perform for each test instance.</param>
        /// <param name="MAX_GROUPS_GENERATED">The maximum number of groups generated according to probabilities.</param>
        static void TestHeuristicWithProbabilities(Random r, Action<OnlineCinema, int> PlaceGroup, int TESTS_PER_INSTANCE = 100, int MAX_GROUPS_GENERATED = 500)
        {
            // Test multiple times for all instances and take the average count of seated people.
            foreach (var instance in testInstances)
            {
                double averageCount = 0d;

                for (int t = 0; t < TESTS_PER_INSTANCE; t++)
                {
                    SetIn(instance);

                    // Read the cinema layout from input data
                    OnlineCinema cinema = ReadOnlineCinemaData();

                    // Keep reading the sizes of arriving groups
                    for (int groupNr = 0; groupNr < MAX_GROUPS_GENERATED; groupNr++)
                    {
                        int groupSize = GenerateGroup(r);
                        PlaceGroup(cinema, groupSize);
                    }

                    averageCount += (1d / (double)TESTS_PER_INSTANCE) * (double)cinema.CountFullSeats();
                }

                Console.WriteLine($"{averageCount}");
            }
        }

        /// <summary>
        /// Tests the Blocked-Chair Heuristic (BCH), but the group input is decided according to probabilities.
        /// </summary>
        /// <param name="r">A Random number generator.</param>
        public static void Test_BCH_P(Random r)
        {
            Console.WriteLine("> Started test:\t\t BCH_P");

            Action<OnlineCinema, int> PlaceGroupDelegate =
                (cinema, groupSize) => OnlinePlanner.PlaceGroup(cinema, groupSize, false);

            TestHeuristicWithProbabilities(r, PlaceGroupDelegate);

            Console.WriteLine("> Finished test:\t BCH_P\n");
        }

        /// <summary>
        /// Tests the Weighted Blocked-Chair Heuristic (WBCH), but the group input is decided according to probabilities.
        /// </summary>
        /// <param name="r">A Random number generator.</param>
        /// <param name="horizontalWeight">The weight for horizontally blocked chairs.</param>
        /// <param name="verticalWeight">The weight for vertically blocked chairs.</param>
        public static void Test_WBCH_P(Random r, int horizontalWeight, int verticalWeight)
        {
            Console.WriteLine("> Started test:\t\t WBCH_P");

            Action<OnlineCinema, int> PlaceGroupDelegate =
                (cinema, groupSize) => OnlinePlanner.PlaceGroupWeighted(cinema, groupSize, horizontalWeight, verticalWeight, false);

            TestHeuristicWithProbabilities(r, PlaceGroupDelegate);

            Console.WriteLine("> Finished test:\t WBCH_P\n");
        }

        /// <summary>
        /// Tests the Random Heuristic (RH), but the group input is decided according to probabilities.
        /// </summary>
        /// <param name="r>A Random number generator</param>
        public static void Test_RH_P(Random r)
        {
            Console.WriteLine("> Started test:\t\t RH_P");

            Action<OnlineCinema, int> PlaceGroupDelegate =
                (cinema, groupSize) => OnlinePlanner.PlaceGroupRandomly(cinema, groupSize, r, false);
            TestHeuristicWithProbabilities(r, PlaceGroupDelegate);

            Console.WriteLine("> Finished test:\t RH_P\n");
        }

        /*
         * Experimenting with the Weighted Blocked-Chair Heuristic.
         */

        /// <summary>
        /// Write results from the experiments to a CSV file.
        /// </summary>
        /// <param name="results">The array of results.</param>
        /// <param name="min_Wh">The inclusive lower bound on weights for horizontally blocked chairs.</param>
        /// <param name="max_Wh">The inclusive upper bound on weights for horizontally blocked chairs.</param>
        /// <param name="min_Wv">The inclusive lower bound on weights for vertically blocked chairs.</param>
        /// <param name="max_Wv">The inclusive lower bound on weights for vertically blocked chairs.</param>
        static void ToCSV(double[,,] results, int min_Wh, int max_Wh, int min_Wv, int max_Wv)
        {
            var csv = new StringBuilder();

            // Create the header.
            csv.Append("Test-instance");
            for (int i = min_Wh; i <= max_Wh; i++)
            {
                for (int j = min_Wv; j <= max_Wv; j++)
                {
                    csv.Append($";Wh={i}&Wv={j}");
                }
            }
            csv.AppendLine();

            // Create rows for each test instance.
            for (int instanceNum = 0; instanceNum < testInstances.Count; instanceNum++)
            {
                csv.Append($"{testInstances[instanceNum]}");
                for (int i = min_Wh; i <= max_Wh; i++)
                {
                    for (int j = min_Wv; j <= max_Wv; j++)
                    {
                        csv.Append($";{results[instanceNum, (i - min_Wh), (j - min_Wv)]}");
                    }
                }
                csv.AppendLine();
            }

            // Write the CSV-string to a file.
            File.WriteAllText("experiment.csv", csv.ToString());
        }

        /// <summary>
        /// Experiment with the Weighted Blocked-Chair Heuristic.
        /// </summary>
        /// <param name="min_Wh">The inclusive lower bound on weights for horizontally blocked chairs.</param>
        /// <param name="max_Wh">The inclusive upper bound on weights for horizontally blocked chairs.</param>
        /// <param name="min_Wv">The inclusive lower bound on weights for vertically blocked chairs.</param>
        /// <param name="max_Wv">The inclusive lower bound on weights for vertically blocked chairs.</param>
        public static void Experiment_WBCH(int min_Wh = 1, int max_Wh = 10, int min_Wv = 1, int max_Wv = 10)
        {
            Console.WriteLine("> Started experiment:\t\t WBCH");

            // Inclusive minimum and maximum bounds on weights to try.

            // Check how many result entries we must make.
            int nrOfHorizontalWeights = max_Wh - min_Wh + 1;
            int nrOfVerticalWeights = max_Wv - min_Wv + 1;

            double[,,] results = new double[testInstances.Count, nrOfHorizontalWeights, nrOfVerticalWeights];

            // Check all combinations of horizontal and vertical weights.
            for (int i = min_Wh; i <= max_Wh; i++)
            {
                for (int j = min_Wv; j <= max_Wv; j++)
                {
                    Console.WriteLine($"Trying:\t Wh = {i}, Wv = {j}");

                    // Test for all instances.
                    for (int instanceNum = 0; instanceNum < testInstances.Count; instanceNum++)
                    {
                        SetIn(testInstances[instanceNum]);

                        // Read the cinema layout from input data
                        OnlineCinema cinema = ReadOnlineCinemaData();

                        // Keep reading the sizes of arriving groups
                        while (true)
                        {
                            int groupSize = InputHelper.ReadIntLine();
                            if (groupSize == 0) break; // A group size of 0 means that there are no more groups
                            OnlinePlanner.PlaceGroupWeighted(cinema, groupSize, i, j, false);
                        }

                        // Save the result.
                        results[instanceNum, (i - min_Wh), (j - min_Wv)] = cinema.CountFullSeats();
                    }
                }
            }

            ToCSV(results, min_Wh, max_Wh, min_Wv, max_Wv);

            Console.WriteLine($"> Finished experiment:\t WBCH");
        }

        /// <summary>
        /// Experiment with the Weighted Blocked-Chair Heuristic, but the group input is decided according to probabilities.
        /// </summary>
        /// <param name="r">A Random number generator.</param>
        /// <param name="min_Wh">The inclusive lower bound on weights for horizontally blocked chairs.</param>
        /// <param name="max_Wh">The inclusive upper bound on weights for horizontally blocked chairs.</param>
        /// <param name="min_Wv">The inclusive lower bound on weights for vertically blocked chairs.</param>
        /// <param name="max_Wv">The inclusive lower bound on weights for vertically blocked chairs.</param>
        /// <param name="TESTS_PER_INSTANCE">The amount of tests to perform for each test instance.</param>
        /// <param name="MAX_GROUPS_GENERATED">The maximum number of groups generated according to probabilities.</param>
        public static void Experiment_WBCH_P(Random r, int min_Wh = 1, int max_Wh = 10, int min_Wv = 1, int max_Wv = 10, int TESTS_PER_INSTANCE = 100, int MAX_GROUPS_GENERATED = 500)
        {
            Console.WriteLine("> Started experiment:\t\t WBCH");

            // Check how many result entries we must make.
            int nrOfHorizontalWeights = max_Wh - min_Wh + 1;
            int nrOfVerticalWeights = max_Wv - min_Wv + 1;

            double[,,] results = new double[testInstances.Count, nrOfHorizontalWeights, nrOfVerticalWeights];

            // Check all combinations of horizontal and vertical weights.
            for (int i = min_Wh; i <= max_Wh; i++)
            {
                for (int j = min_Wv; j <= max_Wv; j++)
                {
                    Console.WriteLine($"Trying:\t Wh = {i}, Wv = {j}");

                    // Test multiple times for all instances and take the average count of seated people.
                    for (int instanceNum = 0; instanceNum < testInstances.Count; instanceNum++)
                    {
                        double averageCount = 0d;

                        for (int t = 0; t < TESTS_PER_INSTANCE; t++)
                        {
                            SetIn(testInstances[instanceNum]);

                            // Read the cinema layout from input data
                            OnlineCinema cinema = ReadOnlineCinemaData();

                            // Keep reading the sizes of arriving groups
                            for (int groupNr = 0; groupNr < MAX_GROUPS_GENERATED; groupNr++)
                            {
                                int groupSize = GenerateGroup(r);
                                OnlinePlanner.PlaceGroupWeighted(cinema, groupSize, i, j, false);
                            }

                            averageCount += (1d / (double)TESTS_PER_INSTANCE) * (double)cinema.CountFullSeats();
                        }

                        results[instanceNum, (i - min_Wh), (j - min_Wv)] = averageCount;
                    }
                }
            }

            ToCSV(results, min_Wh, max_Wh, min_Wv, max_Wv);

            Console.WriteLine($"> Finished experiment:\t WBCH");
        }
    }
}