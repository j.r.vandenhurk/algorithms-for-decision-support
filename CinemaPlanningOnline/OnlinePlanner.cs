﻿using System;
using System.Linq;

namespace CinemaPlanningOnline
{
    static class OnlinePlanner
    {
        /// <summary>
        /// Seats a group according to the Blocked-Chair heuristic.
        /// </summary>
        /// <param name="cinema">The cinema to seat the group in.</param>
        /// <param name="groupSize">The size of the group.</param>
        /// <param name="output">A flag for writing placement output to the commandline.</param>
        public static void PlaceGroup(OnlineCinema cinema, int groupSize, bool output = true)
        {
            PlaceGroupWeighted(cinema, groupSize, 1, 1, output);
        }

        /// <summary>
        /// Seats a group according to the Weighted Blocked-Chair heuristic.
        /// </summary>
        /// <param name="cinema">The cinema to seat the group in.</param>
        /// <param name="groupSize">The size of the group.</param>
        /// <param name="horizontalWeight">The weight for horizontally blocked chairs.</param>
        /// <param name="verticalWeight">The weight for vertically blocked chairs.</param>
        /// <param name="output">A flag for writing placement output to the commandline.</param>
        public static void PlaceGroupWeighted(OnlineCinema cinema, int groupSize, int horizontalWeight, int verticalWeight, bool output = true)
        {
            // Keep track of the best seating option up until now.
            int minScore = Int32.MaxValue;
            int? minRow = null;
            int? minColumn = null;

            // Iterate over all possible seating options and find the best one.
            foreach (var ((row, column), width) in cinema.GetGroupSizesPerChair(groupSize))
            {
                // Score the current option.
                (int, int) blockedChairs = cinema.GetBlockedChairs(row, column, groupSize);
                int score = horizontalWeight * blockedChairs.Item1 + verticalWeight * blockedChairs.Item2;

                // Update the best option if necessary.
                if (score < minScore)
                {
                    minScore = score;
                    minRow = row;
                    minColumn = column;
                }
            }

            // If we found some sort of minimum, place the people. If we didn't find an minimum, we
            // output 0, 0 (we can't place the people)
            // The extra if(output) checks are there since it's much easier to run the tests just printing the totals
            if (minScore != Int32.MaxValue)
            {
                cinema.SetGroupInChairs((int)minRow, (int)minColumn, groupSize);

                if (output)
                {
                    Console.WriteLine($"{minRow + 1} {minColumn + 1}");
                }
            }
            // No suitable seating option was found.
            else if (output)
            {
                Console.WriteLine("0 0");
            }
        }

        /// <summary>
        /// Seats a group randomly.
        /// </summary>
        /// <param name="cinema">The cinema to seat the group in.</param>
        /// <param name="groupSize">The size of the group.</param>
        /// <param name="output">A flag for writing placement output to the commandline.</param>
        public static void PlaceGroupRandomly(OnlineCinema cinema, int groupSize, Random r, bool output = true)
        {
            // Retrieve all options
            ((int, int), int)[] options = cinema.GetGroupSizesPerChair(groupSize).ToArray();

            // In case there are available seating options..
            if (options.Length > 0)
            {
                // Retrieve a random option and its values.
                ((int, int), int) theChosenOne = options[r.Next(0, options.Length)];
                int row = theChosenOne.Item1.Item1;
                int column = theChosenOne.Item1.Item2;
                int width = theChosenOne.Item2;

                // Seat the group.
                cinema.SetGroupInChairs(row, column, groupSize);
                if (output)
                {
                    Console.WriteLine($"{row + 1} {column + 1}");
                }
            }
            // In case there are no available seating options.
            else if (output)
            {
                Console.WriteLine("0 0");
            }
        }
    }
}
